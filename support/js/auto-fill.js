function auto_fill() {
  // array of fields
  var fields = new Array("address","suburb","city","postcode");

  // loop through fields and copy across
  for(var i = 0; i < fields.length; i++) {
    if(document.getElementById("home_" + fields[i]).value != "") {
	  document.getElementById("postal_" + fields[i]).value = document.getElementById("home_" + fields[i]).value;
	}
  }

  document.getElementById("postal_country").selectedIndex = document.getElementById("home_country").selectedIndex;
  document.getElementById("postal_state").value = document.getElementById("home_state").selectedIndex;

}