/** 
    Enrolment Form Email form Validation Version 1.1
	-------------------------------------
    Authors:
    1.0 Oliver Holmes
	1.1 Adam Wolfs
	-------------------------------------
*/

function validate_form() {
  if(document.forms.coe_form.ask_title.value == '0'){
    alert('Title is a required field');
    document.forms.coe_form.ask_title.focus();
    return false;
  }

  if(document.forms.coe_form.ask_lastname.value == ''){
    alert('Your last name is a required field');
    document.forms.coe_form.ask_lastname.focus();
    return false;
  }

  if(document.forms.coe_form.ask_firstname.value == ''){
    alert('Your first name is a required field');
    document.forms.coe_form.ask_firstname.focus();
    return false;
  }

  var dob = document.forms.coe_form.ask_dateofbirth;

  if(dob.value == "") {
    alert("Your date of birth is required");
    dob.focus();
    return false;
  }
  else {
    // regular expression to match required date format
	re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
	if(dob.value != '' && !dob.value.match(re)) {
	  display_date_format();
	  dob.focus();
	  return false;
	} 
  }

  if(document.forms.coe_form.ask_gender.value == '0'){
    alert('Your gender is a required field');
    document.forms.coe_form.ask_gender.focus();
    return false;
  }

  if(document.forms.coe_form.address1_name.value == ''){
    alert('Address is a required field');
    document.forms.coe_form.address1_name.focus();
    return false;
  }

  if(document.forms.coe_form.ask_addresscity1.value == ''){
    alert('Town/City is a required field');
    document.forms.coe_form.ask_addresscity1.focus();
    return false;
  }

  if(document.forms.coe_form.ask_addresspostcode1.value == ''){
    alert('Postcode is a required field');
    document.forms.coe_form.ask_addresspostcode1.focus();
    return false;
  }

  if(document.forms.coe_form.address1_country.value == ''){
    alert('Country is a required field');
    document.forms.coe_form.address1_country.focus();
    return false;
  }

  if(document.forms.coe_form.ask_emailaddress.value == ''){
    alert('Your e-mail address is required');
    document.forms.coe_form.ask_emailaddress.focus();
    return false;
  }
  else {
    re =  /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/; 
    if (!re.test(document.forms.coe_form.ask_emailaddress.value)) {
      alert('Your e-mail address is invalid');
      document.forms.coe_form.ask_emailaddress.focus();
      return false;
    }
  }

  if ((document.forms.coe_form.ask_rpl[0].checked == false) && (document.forms.coe_form.ask_rpl[1].checked == false)) {
    alert('You are required to indicate to AOT whether or not you are applying for RPL.');
    document.forms.coe_form.ask_rpl[0].focus();
    return false;
  }

  if ((document.forms.coe_form.ask_credittransfer[0].checked == false) && (document.forms.coe_form.ask_credittransfer[1].checked == false)) {
    alert('You are required to indicate to AOT whether or not you are applying for Credit Transfer.');
    document.forms.coe_form.ask_credittransfer[0].focus();
    return false;
  }

  if ((document.forms.coe_form.ask_lln[0].checked == false) && (document.forms.coe_form.ask_lln[1].checked == false)) {
    alert('You are required to indicate to AOT whether or not you are applying for LLN Assistance.');
    document.forms.coe_form.ask_lln[0].focus();
    return false;
  }

  if ((document.forms.coe_form.ask_disabilityassistance[0].checked == false) && (document.forms.coe_form.ask_disabilityassistance[1].checked == false)) {
    alert('You are required to indicate to AOT whether or not you are applying for Disability Assistance.');
    document.forms.coe_form.ask_disabilityassistance[0].focus();
    return false;
  }

  if ((document.forms.coe_form.ask_isenglishyourfirstlanguage[0].checked == false) && (document.forms.coe_form.ask_isenglishyourfirstlanguage[1].checked == false)) {
    alert('You are required to indicate to AOT whether or not English is your first language.');
    document.forms.coe_form.ask_isenglishyourfirstlanguage[0].focus();
    return false;
  }

  // Check for agent details
  var ask_nameifagent = document.getElementById("ask_nameifagent");
  var ask_agentemailaddress = document.getElementById("ask_agentemailaddress");
  
  if(ask_nameifagent) {
    if(ask_nameifagent.value == "") {
	  alert('You are required to enter the name of your agent');
	  ask_nameifagent.focus();
	  return false;
	}
  }
  else {
    // Name of agent field does not exist
	// do nothing, continue
  }
  
  if(ask_agentemailaddress) {
    if(ask_agentemailaddress.value == "") {
	  alert('You are required to enter the agents email');
	  ask_agentemailaddress.focus();
	  return false;
	}
  }
  else {
    // Agent ID field does not exist
	// do nothing, continue
  }

  if(document.forms.coe_form.ask_howdidyouhearabouttheonlinediplo.value == '') {
    alert('Please fill out the field that indicates where you heard about AOT');
    document.forms.coe_form.ask_howdidyouhearabouttheonlinediplo.focus();
    return false;
  }

  if (document.forms.coe_form.ask_completedtheenglishlanguage.checked == false) {
    alert('Please check the box that indicates you have completed the application in english.');
    document.forms.coe_form.ask_completedtheenglishlanguage.focus();
    return false;
  }

  if (document.forms.coe_form.ask_checklistcompletedsectionsapplic.checked == false) {
    alert('Please check the box that indicates you have completed all the relevant sections.');
    document.forms.coe_form.ask_checklistcompletedsectionsapplic.focus();
    return false;
  }

  if (document.forms.coe_form.ask_checklisttermsconditions.checked == false) {
    alert('Please check the box that indicates you have read the Student Handbook.');
    document.forms.coe_form.ask_checklisttermsconditions.focus();
    return false;
  }

  if (document.forms.coe_form.ask_declarationinformationtruecheckb.checked == false) {
    alert("Please check the box that indicates that declare's this form is true and correct.");
    document.forms.coe_form.ask_declarationinformationtruecheckb.focus();
    return false;
  }
}

/**
  function display_date_format()
  Displays a generic error message to display the date correctly.
*/
function display_date_format() {
  var dob = document.forms.coe_form.ask_dateofbirth;
  alert("You must display the date in the format of : \n DD/MM/YYYY");
  dob.focus();
}