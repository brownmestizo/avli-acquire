function DisabilityList() {
  if(document.getElementById("ask_disability")) {
    if(document.getElementById("ask_disability").selectedIndex == 1) {
      document.getElementById("disabilitylist").style.display = "block";
    }
    else {
      document.getElementById("disabilitylist").style.display = "none";
	  // set all checkboxes unchecked
	  var set = document.getElementById("disability_group").getElementsByTagName("input");
	  for(i=0; i < set.length; i++) {
        set[i].checked = false;
	  }
    }
  }
}

window.onload = DisabilityList;