function validate_form() {
  // set error to 0
  var error = 0;
  // add or error count if error found
  error += check_form_values();
  // allow the user to continue if no errors found
  return (error > 0) ? false : true;
}

function check_form_values() {
  var error = 0;
  
  ///////////////////////////////////////////
  // grab all selects
  var els = document.getElementsByTagName("select");
  
  // set error msg
      if(els.length>0) {
          errormsg = "Please select an option for the following:\n---------------------------------------------------------------------\n";

          // loop through selects and display on error, using the title attribute
          for (var i = 0; i < els.length; i++) {
    	      // exclusions if statement
        		if(els[i].name != "birthdayDay" && els[i].name != "birthdayMonth" && els[i].name != "birthdayYear") {		
                if(els[i].selectedIndex == 0) {
        	        errormsg += els[i].getAttribute('title') + "\n";
        	        error++;
          		  } else if (els[i].name == "ask_doyouconsideryourselftohaveadisa") {

                  if(els[i].selectedIndex == 1) {
                    var items = document.getElementById("disability_group").getElementsByTagName("input");
                    disChecked = 0;
              			  for (var j = 0; j < items.length; j++) {
                              if(items[j].checked) {
                      				  disChecked++;	
              				        }
              			  }

                      if(disChecked == 0) {
                        errormsg += "Please select a disability\n";
      				          error++;
                      }
      		        }
      		      }
    		    }
          }//end of for loop
      }

    	// check for qualifications
    	if(document.getElementById("qualgroup")) {
    	  // div has been found, check it's child inputs and check for a value.
    	  var items = document.getElementById("qualgroup").getElementsByTagName("input");
          qualChecked = 0;
          for (var k = 0; k < items.length; k++) {
            if(items[k].checked) {
              qualChecked++;	
            }
          }
          if(qualChecked == 0) {
            errormsg += "Please select an option from the qualification list\n";
    		    error++;
          }
    	}


    	// check for course enrolled
    	if(document.getElementById("coursesenrolled")) {
    	  // div has been found, check it's child inputs and check for a value.
    	  var items = document.getElementById("coursesenrolled").getElementsByTagName("input");
          qualChecked = 0;
          for (var l = 0; l < items.length; l++) {
            if(items[l].checked) {
              qualChecked++;	
            }
          }
          if(qualChecked == 0) {
            errormsg += "Please select a course you're enrolled in\n";
    		    error++;
          }
    	}

  ///////////////////////////////////////////
  els = document.getElementsByTagName("input","textarea");
  // set error msg

  if(els.length>0) {
    errormsg += "\nPlease enter feedback for the following:\n---------------------------------------------------------------------\n";

      for (var i = 0; i < els.length; i++) {
            //exclusions if statement
            if(els[i].name != "address2_line2" && els[i].name != "ask_doyouspeakalanguageotherthanengl" 
              && els[i].name != "ask_disabilityhearingdeaf" && els[i].name != "ask_disabilityphysical" 
              && els[i].name != "ask_disabilityintellectual" && els[i].name != "ask_disabilitylearning" 
              && els[i].name != "ask_disabilitymentalillness" && els[i].name != "ask_disabilityacquiredbrainimpairmen" 
              && els[i].name != "ask_disabilityvision" && els[i].name != "ask_disabilitymedicalconsition" 
              && els[i].name != "ask_disabilityothers" && els[i].className != "donotval" && els[i].name != "ask_city1" 
              && els[i].name != "ask_previousothername"
              && els[i].name != "ask_city2") {


              		if(els[i].name == "ask_inwhichyeardidyoucompletethatsch") {
                		  if(document.getElementById("ask_whatisyourhighestcompletedschool") && 
                        document.getElementById("ask_whatisyourhighestcompletedschool").selectedIndex != 6) {
                      //alert(document.getElementById("ask_whatisyourhighestcompletedschool").selectedIndex);
                  		    if(els[i].value == "" || els[i].value == null) {
                                errormsg += els[i].getAttribute('title') + "\n";
                                error++;
                          }
                		  }
              		}

                  else if(els[i].name == "ask_previousfirstname" && document.getElementById("ask_previousname").value == "1") {
                      if(els[i].value == "") {
                          errormsg += els[i].getAttribute('title') + "\n";
                          error++;
                      }
                  }

                  else if(els[i].name == "ask_previouslastname" && document.getElementById("ask_previousname").value == "1") {
                      if(els[i].value == "") {
                          errormsg += els[i].getAttribute('title') + "\n";
                          error++;
                      }
                  }

                  else if(els[i].name == "ask_ifyestopreviousquestionpleasespe") {
                		  if(document.getElementById("ask_doyouspeakalanguageotherthanengl") && 
                        document.getElementById("ask_doyouspeakalanguageotherthanengl").selectedIndex == 2) {
                		    if(els[i].value == "" || els[i].value == null) {
                              errormsg += els[i].getAttribute('title') + "\n";
                              error++;
                        }
      		            }
      		        }

              		else if(els[i].name == "ask_languageotherthanenglish") {
                  		  if(document.getElementById("ask_languageotherthanenglish") && 
                          document.getElementById("ask_doyouspeakalanguageotherthanengl").selectedIndex != 2) {
                    		    if(els[i].value == "" || els[i].value == null) {
                                  errormsg += els[i].getAttribute('title') + "\n";
                                  error++;
                            }
                  		  }
              		}

                  else if(els[i].name == "ask_emailaddress") {

                      var emailExpression = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/
                      if(els[i].value.search(emailExpression) >= 0) {}
                      else {
                          errormsg += els[i].getAttribute('title') + "\n";
                          error++;
                      }

                  }

                  else if(els[i].name == "ask_contactnumber") {
                    var phoneExpression = /^[0-9]{10,20}$/
                    if(els[i].value.search(phoneExpression) >= 0) {}
                    else {
                          errormsg += els[i].getAttribute('title') + "\n";
                          error++;
                    }
                  }

              		else {
                      if (els[i].name != "ask_previousfirstname" && els[i].name != "ask_previouslastname"
                        && els[i].name != "ask_languageotherthanenglish") {
                          if(els[i].value == "" || els[i].value == null) {
                  	        errormsg += els[i].getAttribute('title') + "\n";
                  	        error++;
                          }
                      }
              		}
      	    }

      } //end of for loop
  }

  // display if an error
  if(error>0) alert(errormsg);
  // return error count
  return error;
}