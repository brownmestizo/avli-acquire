<?php
  // Utils v1.0
  // Provides all functions required for the form
  // Written by Adam Wolfs
  // -------------------------------------------------------------------------- //
 
  function array_push_assoc($array, $key, $value){
    $array[$key] = $value;
    return $array;
  }
  
  function check_selected($val,$num) {
    if(isset($_SESSION['data'][$val]) && $_SESSION['data'][$val] == $num) {
	  echo " selected=\"selected\"";
	}
  }
  
  function check_for_content($val) {
    if(isset($_SESSION['data'][$val]) && $_SESSION['data'][$val] != "") {
	  echo $_SESSION['data'][$val];
	}
  }
  
  function check_the_box($val) {
    if(isset($_SESSION['data'][$val]) && $_SESSION['data'][$val] == "true") {
	  echo " checked=\"checked\"";
	}
  }
  
  function destroy_session() {
	session_unset();
	session_destroy();
	$_SESSION['data'] = array();
  }
  
  function sanitise_input($var) {
	// remove tabs
	$var = str_replace("\t",'',$var);
	
	// strip HTML tags from content
	$var = strip_tags($var);
	
	// add slashes to content
	$var = addslashes($var);
	
    // return sanitised data
	return trim($var);
  }
  
  function DateSelector($inName, $useDate=0)  { 
    $monthName = array(1=> "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 

    if($useDate == 0) { 
      $useDate = Time(); 
    }

    echo "<select name=\"" . $inName . "Day\">\n"; 
  
    for($currentDay=1; $currentDay <= 31; $currentDay++) { 
      echo "<option value=\"$currentDay\""; 
      if(isset($_SESSION['data']['birthdayDay']) && $_SESSION['data']['birthdayDay'] == $currentDay) {
  	    echo 'selected="selected"';
	  }
      echo ">";
	  echo $currentDay;
	  echo "</option>\n";
    } 
  
    echo "</select>"; 
  
    echo "<select name=\"" . $inName . "Month\">\n"; 
  
    for($currentMonth = 1; $currentMonth <= 12; $currentMonth++) {
      echo "<option value=\""; 
      echo intval($currentMonth);
      echo "\"";
      if(isset($_SESSION['data']['birthdayMonth']) && $_SESSION['data']['birthdayMonth'] == $currentMonth) {
        echo ' selected="selected"';
	  }
	  echo ">" . $monthName[$currentMonth] . "</option>\n"; 
    }

    echo "</select>"; 

    echo "<select name=\"" . $inName . "Year\">\n"; 
    $startYear = date( "Y", $useDate); 

    for($currentYear = $startYear - 100; $currentYear <= $startYear+5;$currentYear++) {
      echo "<option value=\"$currentYear\"";
	  if(isset($_SESSION['data']['birthdayYear']) && $_SESSION['data']['birthdayYear'] == $currentYear) {
	    echo ' selected="selected"';
	  }
      echo ">$currentYear</option>\n";
    }
    echo "</select>"; 
  }

  function fill_hidden($var) {
    echo '<input type="hidden" title="' . $var . '" name="';
	echo $var;
	echo '" value="';
	if(isset($_SESSION['data'][$var])) {
	  echo $_SESSION['data'][$var];
	}
	echo '" />';
	echo "\n";
  }

  function fill_hidden_direct($data,$fieldname) {
    echo '<input type="hidden" title="' . $fieldname . '" name="';
	echo $fieldname;
	echo '" value="';
    echo ucfirst(strtolower($data));
	echo '" />';
	echo "\n";
  }

  function fill_hidden_clean_name($var) {
    echo '<input type="hidden" title="' . $var . '" name="';
	echo $var;
	echo '" value="';
	if(isset($_SESSION['data'][$var])) {
	  echo ucfirst(strtolower($_SESSION['data'][$var]));
	}
	echo '" />';
	echo "\n";
  }

  function fill_hidden_clean_email($var) {
    echo '<input type="hidden" title="' . $var . '" name="';
	echo $var;
	echo '" value="';
	if(isset($_SESSION['data'][$var])) {
	  echo strtolower($_SESSION['data'][$var]);
	}
	echo '" />';
	echo "\n";
  }

  function fill_hidden_checkbox($var) {

      if(isset($_SESSION['data'][$var])) {
        echo '<input type="hidden" name="';
      	echo $var;
        echo '" value="';

        	if($_SESSION['data'][$var] != "") {
              echo $_SESSION['data'][$var];
        	} else {
        	  echo "false";
        	}

      	echo '" />';
      	echo "\n";
      }
    
  }

  function fill_hidden_dateofbirth($var,$day,$month,$year) {
    echo '<input type="hidden" name="';
	echo $var;
	echo '" value="';
	echo $_SESSION['data'][$day] . "/" . $_SESSION['data'][$month] . "/" . $_SESSION['data'][$year];
	echo '" />';
	echo "\n";
  }
  

  function generate_country_list($fieldname, $type) {
    $type = isset($type) ? $type : '';

    $country = array("12"=>"Australia","1"=>"Afghanistan","2"=>"Albania","3"=>"Algeria","4"=>"American Samoa","5"=>"Andorra","6"=>"Angolo","7"=>"Anguilla","8"=>"Antigua and Burbuda","9"=>"Argentina","10"=>"Armenia","11"=>"Aruba","13"=>"Austria","14"=>"Azerbaijan","15"=>"Bahamas","16"=>"Bahrain","17"=>"Bangladesh","18"=>"Barbados","19"=>"Belarus","20"=>"Belgium","21"=>"Belize","22"=>"Benin","23"=>"Bermuda","24"=>"Bhutan","25"=>"Bolivia","26"=>"Bosnia and Herzegovina","27"=>"Botswana","28"=>"Brazil","29"=>"British Indian Ocean Territory","30"=>"British Virgin Islands","31"=>"Brunei","32"=>"Bulgaria","33"=>"Burkina Faso","34"=>"Burundi","35"=>"Cambodia","36"=>"Cameroon","37"=>"Canada","38"=>"Cape Varde Islands","39"=>"Cayman Islands","40"=>"Central African Republic","41"=>"Chad","42"=>"Chile","43"=>"China","44"=>"Colombia","45"=>"Comoros","46"=>"Congo","47"=>"Cook Islands","48"=>"Costa Rica","49"=>"Cte d'Ivoire","50"=>"Croatia","51"=>"Cuba","52"=>"Cyprus","53"=>"Czech Republic","54"=>"Democratic Republic of the Congo","55"=>"Denmark","56"=>"Djibouti","57"=>"Dominica","58"=>"Dominican Republic","59"=>"East Timor","60"=>"Ecuador","61"=>"Egypt","62"=>"El Salvador","63"=>"Equatorial Guinea","64"=>"Eritrea","65"=>"Estonia","66"=>"Ethiopia","67"=>"Falkland Islands","68"=>"Fiji","69"=>"Finland","70"=>"France","71"=>"French Guiana","72"=>"French Plynesia","73"=>"Gabon","74"=>"Gambia","75"=>"Georgia","76"=>"Germany","77"=>"Ghana","78"=>"Gibraltar","79"=>"Greece","80"=>"Greenland","81"=>"Grenada","82"=>"Guadeloupe","83"=>"Guam","84"=>"Guatemala","85"=>"Guinea","86"=>"Guinea-Bissau","87"=>"Guyana","88"=>"Haiti","89"=>"Hinduras","90"=>"Hungry","91"=>"Iceland","92"=>"india","93"=>"Indonesia","94"=>"Iran","95"=>"Iraq","96"=>"Ireland","97"=>"Isreal","98"=>"Italy","99"=>"Jamaica","100"=>"Japan","101"=>"Jordan","102"=>"Kazakhstan","103"=>"Kenya","104"=>"Kiribati","105"=>"Korea, North","106"=>"Korea, South","107"=>"Kuwait","108"=>"Kyrgyzstan","109"=>"Laos","110"=>"Latvia","111"=>"Lebanon","112"=>"Lesotho","113"=>"Liberia","114"=>"Libya","115"=>"Liechtenstein","116"=>"Lithuania","117"=>"Luxembourg","118"=>"Macedonia","119"=>"Madagascar","120"=>"Malawi","121"=>"Malaysia","122"=>"Maldives","123"=>"Mali","124"=>"Malta","125"=>"Marshall Islands","126"=>"Martinique","127"=>"Mauritania","128"=>"Mauritius","129"=>"Mayotte","130"=>"Mexico","131"=>"Micronesia","132"=>"Moldova","133"=>"Monacco","134"=>"Mongolia","135"=>"Montserrat","136"=>"Morocco","137"=>"Mozambique","138"=>"Myanmar","139"=>"Namibia","140"=>"Nauru","141"=>"Nepal","142"=>"Netherlands","143"=>"Netherlands Antilles","144"=>"New Caledonia","145"=>"New Zealand","146"=>"Nicaragua","147"=>"Niger","148"=>"Nigeria","149"=>"Niue","150"=>"Norfolk Island","151"=>"Northern Mariana Islands","152"=>"Norway","153"=>"Oman","154"=>"Pakistan","155"=>"Palau","156"=>"Panama","157"=>"Papua New Guinea","158"=>"Paraguay","159"=>"Pelestinian West Bank and Gaza","160"=>"Peru","161"=>"Philippines","162"=>"Pitcaim","163"=>"Poland","164"=>"Portugal","165"=>"Puerto Rico","166"=>"Qatar","167"=>"Runion","168"=>"Romania","169"=>"Russia","170"=>"Rwanda","171"=>"Saint Helena","172"=>"Saint Kitts and Nevis","173"=>"Saint Lucia","174"=>"Saint Pierre and Miquelon","175"=>"Saint Vincent and the Grenadines","176"=>"Samoa","177"=>"San Marino","178"=>"So Tom e Prncipe","179"=>"Saudi Arabia","180"=>"Senegal","181"=>"Serbia and Montenegro","182"=>"Seychelles","183"=>"Sierra Leone","184"=>"Singapore","185"=>"Slovakia","186"=>"Slovenia","187"=>"Solomon Islands","188"=>"Somalia","189"=>"South Africa","190"=>"Spain","191"=>"Sri Lanka","192"=>"Sudan","193"=>"Suriname","194"=>"Swaziland","195"=>"Swedan","196"=>"Switzerland","197"=>"Syria","198"=>"Taiwan","199"=>"Tajikistan","200"=>"Tanzania","201"=>"Thailand","202"=>"Togo","203"=>"Tokelau","204"=>"Tonga","205"=>"Trinidad and Tobago","206"=>"Tunisia","207"=>"Turkey","208"=>"Turkmenistan","209"=>"Turks and Caicos Islands","210"=>"Tuvalu","211"=>"U.S. Virgin Islands","212"=>"Uganda","213"=>"Ukraine","214"=>"United Arab Emirates","215"=>"United Kingdom","216"=>"Uruguay","217"=>"USA","218"=>"Uzbekistan","219"=>"Vanuatu","220"=>"Vatican State","221"=>"Venezuela","222"=>"Viet Nam","223"=>"Wallis and Futuna","224"=>"Yeman","225"=>"Zambia","226"=>"Zimbabwe");
  
    foreach($country as $key=>$value) {
  	  if($type == "txt") {
          echo '<option value="'.$value.'"'.check_selected($fieldname,$value).'>'.$value.'</option>';
  	  } else {
          echo '<option value="'.$key.'"'.check_selected($fieldname,$key).'>'.$value.'</option>';
  	  }
    }
  }

?>