<?php
  session_start();
  // Utility Functions
  include("utils.php");
 
  if(!isset($_SESSION['data'])) {
    $_SESSION['data'] = array('active'=>'1');
  }
  
  // check if post is not empty
  if(sizeof($_POST) > 0) {
    // loop through post data
    foreach($_POST as $key=>$value) {		
	  if(!array_key_exists($key,$_SESSION['data'])) {
		// add new fields to end of array
	    $_SESSION['data'] = array_push_assoc($_SESSION['data'],$key,sanitise_input($value));
	  }
	  else {
		// update values if found
	    $_SESSION['data'][$key] = sanitise_input($value);
	  }
	}
  }
  
  // remove 'submit' keys from data
  if(array_key_exists("submit",$_SESSION['data'])) {
    unset($_SESSION['data']["submit"]);
  } 
?>