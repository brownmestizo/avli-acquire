<?php include("support/includes/session.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AOT - Student Enrolment - Step 2</title>
<script type="text/javascript" src="support/js/val.js"></script>
<link href="support/css/layout.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper" class="step2">


<div style=""><img src="images/avli-logo.png" width="203" height="54" title="AVLI logo"/></div>

<br />

  <h1>Student Enrolment Form (2/3)</h1>
  <div id="logo"></div>
  <table width="100%" border="0">
    <tr>
      <td width="100%" valign="bottom"><h2>Language and Cultural Diversity</h2></td>
    </tr>
  </table>
    <form action="step3.php" method="post" name="secondpage" onsubmit="return validate_form();">
    <fieldset class="on">
      <table width="100%" border="0">
        <tr>
          <th>In which country were you born?</th>
          <td>
			<select name="ask_countryofbirth" id="ask_countryofbirth" title="In which country were you born?">
              <option value="">Click to select an Country</option>
              <?php generate_country_list("ask_addresscountry1","txt"); ?>
            </select>          
          </td>
        </tr>
        <tr>
          <th>Do you speak a language other than English at home?</th>
          <td><select name="ask_doyouspeakalanguageotherthanengl" title="Do you speak a language other than English at home?" id="ask_doyouspeakalanguageotherthanengl">
              <option value="">Click to select an option</option>              
              <option value="2"<?php check_selected("ask_doyouspeakalanguageotherthanengl","2"); ?>>Yes, other - Please specify</option>
              <option value="1"<?php check_selected("ask_doyouspeakalanguageotherthanengl","1"); ?>>English Only</option>              
            </select></td>
        </tr>
        <tr>
          <th>If yes to previous question, please specify</th>
          <td><input type="text" name="ask_languageotherthanenglish" id="ask_languageotherthanenglish" title="What other language do you speak at home other than English?" value="<?php check_for_content("ask_languageotherthanenglish"); ?>" /></td>
        </tr>
        <tr>
          <th>How well do you speak English?</th>
          <td><select name="ask_languagehowwelldoyouspeakenglish" title="How well do you speak English?">
              <option value="">Click to select an option</option>
              <option value="1"<?php check_selected("ask_languagehowwelldoyouspeakenglish","1"); ?>>Very well</option>
              <option value="2"<?php check_selected("ask_languagehowwelldoyouspeakenglish","2"); ?>>Well</option>
              <option value="3"<?php check_selected("ask_languagehowwelldoyouspeakenglish","3"); ?>>Not well</option>
              <option value="4"<?php check_selected("ask_languagehowwelldoyouspeakenglish","4"); ?>>Not at all</option>
            </select></td>
        </tr>
        <tr>
          <th>Are you of Aboriginal or Torres Strait Islander origin?</th>
          <td><select name="ask_indigenousstatus" title="Are you of Aboriginal or Torres Strait Islander origin?">
              <option value="">Click to select an option</option>
              <option value="4"<?php check_selected("ask_indigenousstatus","4"); ?>>No</option>
              <option value="1"<?php check_selected("ask_indigenousstatus","1"); ?>>Yes, Aboriginal</option>
              <option value="2"<?php check_selected("ask_indigenousstatus","2"); ?>>Yes, Torres Strait Islander</option>
              <option value="3"<?php check_selected("ask_indigenousstatus","3"); ?>>Yes, Aboriginal and Torres Strait Islander</option>
            </select></td>
        </tr>
      </table>
    </fieldset>
    <br />
    <table width="100%" border="0">
      <tr>
        <td width="100%" valign="bottom"><h2>Previous Qualifications Achieved</h2></td>
      </tr>
    </table>
    <fieldset class="odd">
      <table width="100%" border="0" class="textbox">
        <tr>
          <th>Have you SUCCESSFULLY completed any of the following qualifications?</th>
          <td>
          <div id="qualgroup">
          <label for="ask_qualbachelordegreeorhigherdegree">
              <input type="checkbox" id="ask_qualbachelordegreeorhigherdegree" name="ask_qualbachelordegreeorhigherdegree" class="checkbox" <?php check_the_box("ask_qualbachelordegreeorhigherdegree"); ?> value="true" />
              Bachelor Degree or Higher Degree</label>
              
            <label for="ask_qualadvanceddiplomaorassociatede">
              <input type="checkbox" id="ask_qualadvanceddiplomaorassociatede" name="ask_qualadvanceddiplomaorassociatede" class="checkbox" <?php check_the_box("ask_qualadvanceddiplomaorassociatede"); ?> value="true" />
              Advanced Diploma or Associate Degree</label>
              
            <label for="ask_qualdiploma">
              <input type="checkbox" id="ask_qualdiploma" name="ask_qualdiploma" class="checkbox" <?php check_the_box("ask_qualdiploma"); ?> value="true" />
              Diploma (or Associate Diploma)</label>
              
            <label for="ask_qualcertificateiv">
              <input type="checkbox" id="ask_qualcertificateiv" name="ask_qualcertificateiv" class="checkbox" <?php check_the_box("ask_qualcertificateiv"); ?> value="true" />
              Certificate IV (or Advanced Certificate/Technician)</label>
              
            <label for="ask_qualcertificateiii">
              <input type="checkbox" id="ask_qualcertificateiii" name="ask_qualcertificateiii" class="checkbox" <?php check_the_box("ask_qualcertificateiii"); ?> value="true" />
              Certificate III (or Trade Certificate)</label>
              
            <label for="ask_qualcertificateii">
              <input type="checkbox" id="ask_qualcertificateii" name="ask_qualcertificateii" class="checkbox" <?php check_the_box("ask_qualcertificateii"); ?> value="true" />
              Certificate II</label>
              
            <label for="ask_qualcertificatei">
              <input type="checkbox" id="ask_qualcertificatei" name="ask_qualcertificatei" class="checkbox" <?php check_the_box("ask_qualcertificatei"); ?> value="true" />
              Certificate I</label>
              
            <label for="ask_qualothers">
              <input type="checkbox" id="ask_qualothers" name="ask_qualothers" class="checkbox" <?php check_the_box("ask_qualothers"); ?> value="true" />
              Certificates other than the above</label>
              
            <label for="ask_qualnoneoftheabove">
              <input type="checkbox" id="ask_qualnoneoftheabove" name="ask_qualnoneoftheabove" class="checkbox" <?php check_the_box("ask_qualnoneoftheabove"); ?> value="true" onclick="javascript:toggle('checkbox');" />
              None of the above</label>
              
            </div>
              </td>
        </tr>
      </table>
    </fieldset>
    <br />
    <p align="right" style="padding-right: 34px;">
      <input type="submit" value="Continue to Page 3 &raquo;" name="submit" class="submit" />
    </p>
    <br />
    <br />
  </form>
</div>
<hr class="general" />
</body>
</html>