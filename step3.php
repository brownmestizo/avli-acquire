<?php include("support/includes/session.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AOT - Student Enrolment - Step 3</title>

<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function($) {
    
  $('#ask_whatisyourhighestcompletedschool').change(function() {
    if ($(this).val() == "1") $('#year12').slideDown("fast");    
    else $('#year12').slideUp("fast"); // hide div if value is not "custom"         
  });   
    
 
}); 
</script>

<link href="support/css/layout.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="support/js/val.js"></script>
</head>
<body class="step2">
<div id="wrapper">

<div style=""><img src="images/avli-logo.png" width="203" height="54" title="AVLI logo"/></div>

<br />
  <h1>Student Enrolment Form (3/3)</h1>
  <div id="logo"></div>
    <form method="post" action="http://www.accreditedtraining.com.au/cgi-bin/acquire.pl" name="coe_form" onsubmit="return validate_form();">
    <br />
    <table width="100%" border="0">
      <tr>
        <td width="100%" valign="bottom"><h2>Disability</h2></td>
      </tr>
    </table>
    <fieldset class="on">
      <table width="100%" border="0">
        <tr>
          <th>Do you consider yourself to have a disability, impairment or long-term condition?</th>
          <td><select name="ask_disabilitydoyouconsideryourselft" title="Do you consider yourself to have a disability, impairment or long-term condition?" onchange="DisabilityList();" id="ask_disability">
              <option value="">Click to select an option</option>
              <option value="1"<?php check_selected("ask_disabilitydoyouconsideryourselft","1"); ?>>Yes</option>
              <option value="2"<?php check_selected("ask_disabilitydoyouconsideryourselft","2"); ?>>No</option>
            </select></td>
        </tr>
        </table>
        <table width="100%" border="0" id="disabilitylist" style="display: none;">
        <tr>
          <th>If YES, then please indicate the area/s of disability, impairment or long-term condition:</th>
          <td>
          <div id="disability_group">
          <label for="ask_disabilityhearingdeaf">
              <input type="checkbox" id="ask_disabilityhearingdeaf" name="ask_disabilityhearingdeaf" class="checkbox" <?php check_the_box("ask_disabilityhearingdeaf"); ?> value="true" />
              Hearing/Deaf</label>
            <label for="ask_disabilityphysical">
              <input type="checkbox" id="ask_disabilityphysical" name="ask_disabilityphysical" class="checkbox" <?php check_the_box("ask_disabilityphysical"); ?> value="true" />
              Physical</label>
            <label for="ask_disabilityintellectual">
              <input type="checkbox" id="ask_disabilityintellectual" name="ask_disabilityintellectual" class="checkbox" <?php check_the_box("ask_disabilityintellectual"); ?> value="true" />
              Intellectual</label>
            <label for="ask_disabilitylearning">
              <input type="checkbox" id="ask_disabilitylearning" name="ask_disabilitylearning" class="checkbox" <?php check_the_box("ask_disabilitylearning"); ?> value="true" />
              Learning</label>
            <label for="ask_disabilitymentalillness">
              <input type="checkbox" id="ask_disabilitymentalillness" name="ask_disabilitymentalillness" class="checkbox" <?php check_the_box("ask_disabilitymentalillness"); ?> value="true" />
              Mental Illness</label>
            <label for="ask_disabilityacquiredbraininjury">
              <input type="checkbox" id="ask_disabilityacquiredbraininjury" name="ask_disabilityacquiredbraininjury" class="checkbox" <?php check_the_box("ask_disabilityacquiredbraininjury"); ?> value="true" />
              Acquired Brain Impairment</label>
            <label for="ask_disabilityvision">
              <input type="checkbox" id="ask_disabilityvision" name="ask_disabilityvision" class="checkbox" <?php check_the_box("ask_disabilityvision"); ?> value="true" />
              Vision</label>
            <label for="ask_disabilitymedicalcondition">
              <input type="checkbox" id="ask_disabilitymedicalcondition" name="ask_disabilitymedicalcondition" class="checkbox" <?php check_the_box("ask_disabilitymedicalcondition"); ?> value="true" />
              Medical Condition</label>
            <label for="ask_disabilityother">
              <input type="checkbox" id="ask_disabilityother" name="ask_disabilityother" class="checkbox" <?php check_the_box("ask_disabilityother"); ?> value="true" />
              Other</label>
            </div>
            </td>
        </tr>
      </table>
    </fieldset>
    <br />
    <table width="100%" border="0">
      <tr>
        <td width="100%" valign="bottom"><h2>Schooling</h2></td>
      </tr>
    </table>
    <fieldset class="odd">
      <table width="100%" border="0">
        <tr>
          <th>What is your highest COMPLETED school level?</th>
          <td><select name="ask_whatisyourhighestcompletedschool" title="What is your highest COMPLETED school level?" id="ask_whatisyourhighestcompletedschool">
              <option value="">Click to select an option</option>
              <option value="1"<?php check_selected("ask_whatisyourhighestcompletedschool","1"); ?>>Year 12 or equivalent</option>
              <option value="2"<?php check_selected("ask_whatisyourhighestcompletedschool","2"); ?>>Year 11 or equivalent</option>
              <option value="3"<?php check_selected("ask_whatisyourhighestcompletedschool","3"); ?>>Year 10 or equivalent</option>
              <option value="4"<?php check_selected("ask_whatisyourhighestcompletedschool","4"); ?>>Year 9 or equivalent</option>
              <option value="5"<?php check_selected("ask_whatisyourhighestcompletedschool","5"); ?>>Year 8 or below</option>
              <option value="6"<?php check_selected("ask_whatisyourhighestcompletedschool","6"); ?>>Never attended school</option>
            </select></td>
        </tr>
        <tr>
          <th>In which YEAR did you complete that school level?</th>
          <td><input type="text" name="ask_inwhichyeardidyoucompletethatsch" id="ask_inwhichyeardidyoucompletethatsch" title="In which YEAR did you complete that school level?" value="<?php check_for_content("ask_inwhichyeardidyoucompletethatsch"); ?>" /></td>
        </tr>
        <tr>
          <th>Are you still attending secondary school?</th>
          <td><select name="ask_areyoustillattendingsecondarysch" id="ask_areyoustillattendingsecondarysch" title="Are you still attending secondary school?">
              <option value="">Click to select an option</option>
              <option value="1"<?php check_selected("ask_areyoustillattendingsecondarysch","1"); ?>>Yes</option>
              <option value="2"<?php check_selected("ask_areyoustillattendingsecondarysch","2"); ?>>No</option>
            </select></td>
        </tr>
      </table>
    </fieldset>
    <br />

    <div class="invisible" id="year12">
    <div class="fieldtab">
    <h2>Previous Names</h2></div>
       <fieldset class="odd">
      <table width="100%" border="0">
        <tr>
          <th>School Student Number</th>
          <td><input type="text" name="ask_schoolstudentnumber" title="Please enter your student number" id="ask_schoolstudentnumber" value="" /></td>
        </tr>
        <tr>
          <th>School Name</th>
          <td><input type="text" name="ask_schoolname" title="Please enter your school name" id="ask_schoolname" value="" /></td>
        </tr>
        <tr>
          <th>State</th>
          <td>
            <select name="ask_schoolstate" title="Please enter your school state" id="ask_schoolstate" >
              <option value="">Click to select a state</option>
              <option value="8"<?php check_selected("ask_schoolstate","8"); ?>>Australian Capital Territory</option>
              <option value="2"<?php check_selected("ask_schoolstate","2"); ?>>New South Wales</option>
              <option value="3"<?php check_selected("ask_schoolstate","3"); ?>>Northern Territory</option>
              <option value="4"<?php check_selected("ask_schoolstate","4"); ?>>Queensland</option>
              <option value="8"<?php check_selected("ask_schoolstate","5"); ?>>South Australia</option>
              <option value="6"<?php check_selected("ask_schoolstate","6"); ?>>Tasmania</option>
              <option value="1"<?php check_selected("ask_schoolstate","1"); ?>>Victoria</option>
              <option value="7"<?php check_selected("ask_schoolstate","7"); ?>>Western Australia</option>
            </select>
          </td>
        </tr>
      </table>
   
    </fieldset>
    </div>
    

    <fieldset class="on">
      <table width="100%" border="0">
        <tr>
          <td width="100%" valign="bottom"><h2>Employment</h2></td>
        </tr>
      </table>
      <table width="100%" border="0" class="textfields">
        <tr>
          <td>Of the following categories, which BEST describes your current employment status?</td>
        </tr>
        <tr>
          <td><select name="ask_employment" id="ask_employment" title="Of the following categories, which BEST describes your current employment status?">
              <option value="">Click to select an option</option>
              <option value="1"<?php check_selected("ask_employment","1"); ?>>Full-time employee</option>
              <option value="2"<?php check_selected("ask_employment","2"); ?>>Part-time employee</option>
              <option value="3"<?php check_selected("ask_employment","3"); ?>>Self employed - not employing others</option>
              <option value="4"<?php check_selected("ask_employment","4"); ?>>Employer</option>
              <option value="5"<?php check_selected("ask_employment","5"); ?>>Employed - unpaid worker in a family business</option>
              <option value="6"<?php check_selected("ask_employment","6"); ?>>Unemployed - seeking full-time work</option>
              <option value="7"<?php check_selected("ask_employment","7"); ?>>Unemployed - seeking part-time work</option>
              <option value="8"<?php check_selected("ask_employment","8"); ?>>Not employed - not seeking employment</option>
            </select></td>
        </tr>
      </table>
    </fieldset>
    <br />
    <table width="100%" border="0">
      <tr>
        <td width="100%" valign="bottom"><h2>Study Reason</h2></td>
      </tr>
    </table>
    <fieldset class="odd">
      <table width="100%" border="0">
        <tr>
          <td>Of the following categories, which BEST describes your main reason for undertaking this course / traineeship / apprenticeship</td>
        </tr>
        <tr>
          <td height="23"><select name="ask_studyreasons" title="Of the following categories, which BEST describes your main reason for undertaking this course / traineeship / apprenticeship">
              <option value="">Click to select an option</option>
              <option value="1"<?php check_selected("ask_studyreasons","1"); ?>>To get a job</option>
              <option value="2"<?php check_selected("ask_studyreasons","2"); ?>>To develop my existing business</option>
              <option value="3"<?php check_selected("ask_studyreasons","3"); ?>>To start my own business</option>
              <option value="4"<?php check_selected("ask_studyreasons","4"); ?>>To try for a different career</option>
              <option value="5"<?php check_selected("ask_studyreasons","5"); ?>>To get a better job or promotion</option>
              <option value="6"<?php check_selected("ask_studyreasons","6"); ?>>It was a requirement of my job</option>
              <option value="7"<?php check_selected("ask_studyreasons","7"); ?>>To get into another course of study</option>
              <option value="8"<?php check_selected("ask_studyreasons","8"); ?>>For personal interest or self-development</option>
              <option value="9"<?php check_selected("ask_studyreasons","9"); ?>>Other reasons</option>
            </select></td>
        </tr>
      </table>
    </fieldset>
    <br />
    
    
    
    <table width="100%" border="0">
      <tr>
        <td width="100%" valign="bottom"><h2>Studying in an Electronic Environment</h2></td>
      </tr>
    </table>
    
    
    
         <textarea name="ask_electricenvironment" id="ask_electricenvironment" cols="45" rows="5" readonly="readonly" style="display:none;">Studying in an Electronic Environment

For your study in the AVLI online environment, it is important for you to understand the legislation that governs electronic communications in the context of the Vocational Educational Training sector:
. Electronic Transactions Act 1999
. Higher Education Support Act 2003
. VET Guidelines

Wherever permitted by law, AVLI will send and receive communication through electronic means that are accessible and authorised for students to use.

In applying for admission, during enrolment and while studying in an AVLI course, the following applies to each individual:

. All applicants seeking to enrol into a course with AVLI consent to giving and being given information by way of electronic communication.
. All applicants seeking to enrol into a course with AVLI consent to fulfilling the electronic signature requirements during the application, enrolment and study, by:
    .providing AVLI with means of personal identification as requested at the time, this may be in the form of a photo ID or through provision of a unique applicant identification number as instructed by AVLI. 


. All applicants consent to AVLI’s use of the personal identification for the purpose it was collected at the time.
. All applicants seeking to enrol into a course with AVLI consent to AVLI producing electronic documents, where and in the form permitted by Commonwealth law. 
. All applicants will be required to provide AVLI with a current copy of their photo ID, which also displays current address and date of birth, to complete the enrolment process. 


</textarea>

    <fieldset class="odd">
      <table width="100%" border="0">
        <tr>
          <td><p>For your study in the AVLI online  environment, it is important for you to understand the legislation that governs  electronic communications in the context of the Vocational Educational Training  sector:</p>
            <ul type="disc">
              <li>Electronic Transactions Act 1999</li>
              <li>Higher Education Support Act 2003</li>
              <li>VET Guidelines</li>
            </ul>
            <p>Wherever  permitted by law, AVLI will send and receive communication through electronic  means that are accessible and authorised for students to use.</p>
            <p>In  applying for admission, during enrolment and while studying in an AVLI course,  the following applies to each individual:</p>
            <ul type="disc">
              <li>All applicants seeking to enrol into a course with AVLI consent to giving and being given information by way of <strong>electronic communication</strong>.</li>
              <li>All applicants seeking to enrol into a course with AVLI consent to fulfilling the <strong>electronic signature </strong>requirements during the application, enrolment and study, by:</li>
              <ul type="circle">
                <li>providing AVLI with means of personal identification as requested at the time, this may be in the form of a photo ID or through provision of a unique applicant identification number as instructed by AVLI. </li>
              </ul>
            </ul>
            <ul type="disc">
              <li>All applicants consent to AVLI’s use of the personal identification for the purpose it was collected at the time.</li>
            </ul>
            <ul type="disc">
              <li>All applicants seeking to enrol into a course with AVLI consent to AVLI producing <strong>electronic documents</strong>, where and in the form permitted by Commonwealth law. </li>
              <li>All applicants will be required to provide AVLI with a current copy of their photo ID, which also displays current address and date of birth, to complete the enrolment process.  </li>
            </ul>
</td>
        </tr>
        
      </table>
    </fieldset>
    <br />
    
    
        <fieldset class="on">
        
        
        <textarea name="ask_applicantdeclaration" id="ask_applicantdeclaration" cols="45" rows="5" readonly="readonly" style="display:none;">Applicant declaration

       By submitting this form, I acknowledge and accept the following declaration:

    . The information provided by me in this form is complete, true and correct and that my submitting this electronic form constitutes a legal electronic signature on this enrolment form under current Australian law;
    . I have read, understood and agree to the information contained in the Student Handbook as published on the AVLI website;
    . I have read, understood and agree to the terms and conditions contained in the terms and conditions published on the AVLI website;
    . I am an Australian citizen, or the holder of a permanent humanitarian visa who will be resident in Australia for the duration of the VET unit of study;
    . I will provide AVLI with the required documentation as proof of being an Australian citizen or permanent humanitarian visa holder; and
    . I will provide AVLI with current Photo ID which depicts me and is a valid document for which I give permission to AVLI to use to verify my identity for the purpose of course eligibility and visual identification during my course enrolment.
         


</textarea>
        
        
        
      <table width="100%" border="0">
      <tr>
        <td width="100%" valign="bottom"><h2>APPLICANT DECLARATION</h2></td>
      </tr>
        <tr>
          <td><p>By submitting this  form, I acknowledge and accept the following declaration: </p>
            <ul>
              <li>The  information provided by me in this form is complete, true and correct and that  my submitting this electronic form constitutes a legal electronic signature on  this enrolment form under current Australian law; </li>
              <li>I have  read, understood and agree to the information contained in the Student Handbook  as published on the AVLI website; </li>
              <li>I have  read, understood and agree to the terms and conditions contained in the terms  and conditions published on the AVLI website; </li>
              <li>I am an  Australian citizen, or the holder of a permanent humanitarian visa who will be  resident in Australia for the duration of the VET unit of study;</li>
              <li>I will  provide AVLI with the required documentation as proof of being an Australian  citizen or permanent humanitarian visa holder; and</li>
              <li>I will  provide AVLI with current Photo ID which depicts me and is a valid document for  which I give permission to AVLI to use to verify my identity for the purpose of  course eligibility and visual identification during my course enrolment. </li>
            </ul>
            <table width="100%" border="0">
              <tr>
                <th style="text-align:left">Applicant ID
                  <input type="text" name="ask_applicantID" id="ask_applicantID" title="Please enter your applicant ID" value="<?php check_for_content("ask_applicantID"); ?>" />
                  &nbsp;&nbsp;&nbsp;&nbsp;<a href="#"><a href="#">What is this?<span class="tooltip">Applicant ID is created by you, using your details: <br />
                  First 3 letters of SURNAME<br />
                  First 3 letters for FIRST NAME <br />
                  Month and Year of birth - MMYY <br />
  <b>Eg: SMIJOH0165 (John Smith born January 1965)</b> <br />
                  Surname and/or First Name with less than 3 letters – substitute an X <br />
  <b>Eg: QIXLUX0383 (Lu Qi born March 1983)</b> </span></a></th>
              </tr>
            </table>
            <p>&nbsp;</p>
</td>
        </tr>
        
      </table>
    </fieldset>
    <p>&nbsp;</p>
    
    
    <!-- Hidden fields -->
    <!-- CRM fields -->
    <input name="subject" value="VET FEE Acquire" type="hidden" />
    <input name="recipient" value="leadqueue@aot.edu.au" type="hidden" />
    <input name="redirect" value="http://avli.com.au/acquire-avli/enrolment/thanks.php" type="hidden" />
    <input name="askcrm_subject" value="VET FEE Acquire" type="hidden"/>
    <input name="ask_category" value="1" type="hidden"/>

    <!-- page one fields -->
    <?php fill_hidden_clean_name("ask_title"); ?>
    <?php fill_hidden_clean_name("ask_firstname"); ?>
    <?php fill_hidden_clean_name("ask_lastname"); ?>
    <?php fill_hidden_dateofbirth("ask_dateofbirth","birthdayDay","birthdayMonth","birthdayYear"); ?>
    <?php fill_hidden("ask_gender"); ?>
    <?php fill_hidden_clean_email("ask_emailaddress"); ?>
    <?php fill_hidden("ask_contactnumber"); ?>
    <?php fill_hidden("ask_address1"); ?>
    <?php fill_hidden("ask_addresssuburb1"); ?>
    <?php fill_hidden_clean_name("ask_addresscity1"); ?>
    <?php fill_hidden("ask_addressstate1"); ?>
    <?php fill_hidden("ask_addresspostcode1"); ?>
    <?php fill_hidden("ask_address2"); ?>
    <?php fill_hidden("ask_addresssuburb2"); ?>
    <?php fill_hidden_clean_name("ask_addresscity2"); ?>
    <?php fill_hidden("ask_addressstate2"); ?>
    <?php fill_hidden("ask_addresspostcode2"); ?>
    <?php fill_hidden("ask_addresscountry1"); ?>
    <?php fill_hidden("ask_addresscountry2"); ?>
    <?php fill_hidden("ask_previousname"); ?>
    <?php fill_hidden("ask_previousfirstname"); ?>
    <?php fill_hidden("ask_previouslastname"); ?>
    <?php fill_hidden("ask_previousothername"); ?>
    <?php fill_hidden("ask_vetfeecourseslist"); ?>
        
    
    <!-- page 2 fields - Language and Cultural Diversity-->
    <?php fill_hidden("ask_countryofbirth"); ?>    
    <?php fill_hidden("ask_doyouspeakalanguageotherthanengl"); ?>
    <?php fill_hidden("ask_languageotherthanenglish"); ?>
    <?php fill_hidden("ask_languagehowwelldoyouspeakenglish"); ?>
    <?php fill_hidden("ask_indigenousstatus"); ?>
    
    <!-- page 2 fields - Previous Qualifications Achieved -->
    <?php fill_hidden_checkbox("ask_qualbachelordegreeorhigherdegree"); ?>
    <?php fill_hidden_checkbox("ask_qualadvanceddiplomaorassociatede"); ?>
    <?php fill_hidden_checkbox("ask_qualdiploma"); ?>
    <?php fill_hidden_checkbox("ask_qualcertificateiv"); ?>
    <?php fill_hidden_checkbox("ask_qualcertificateiii"); ?>
    <?php fill_hidden_checkbox("ask_qualcertificateiI"); ?>
    <?php fill_hidden_checkbox("ask_qualcertificatei"); ?>
    <?php fill_hidden_checkbox("ask_qualothers"); ?>
    <?php fill_hidden_checkbox("ask_qualnoneoftheabove"); ?>
    
    <!-- End hidden fields -->


    <p align="right" style="padding-right: 34px;">
      <input type="submit" value="Click to submit form &raquo;" name="submit" class="submit" />
    </p>
    <br />
  </form>
</div>
<hr class="general" />
<script type="text/javascript" src="support/js/disability.js"></script>
</body>
</html>