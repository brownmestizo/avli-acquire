<?php include("support/includes/session.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AOT - Student Enrolment - Finished!</title>
<link href="support/css/layout.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="support/js/val.js"></script>
</head>
<body class="step2">
<div id="wrapper">

<div style=""><img src="images/avli-logo.png" width="203" height="54" title="AVLI logo"/></div>

<br />
  <h1>Student Enrolment Form (Complete)</h1>
  <div id="logo"></div>
  <br />
  <h2>Thank you!</h2>
  <p>Thank you for completing the online enrolment process in your chosen course and congratulations on taking the first step toward enhancing your future. We appreciate the time you have taken and look forward to assisting you in getting started in your course.</p><br />

  <?php
    destroy_session();
  ?>


</div>
<hr class="general" />
</body>
</html>