<?php include("support/includes/session.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AOT - Student Enrolment - Step 1</title>
<script type="text/javascript" src="support/js/val.js"></script>

<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function($) {
 	  
	$('#ask_previousname').change(function() {
		if ($(this).val() == '1') $('#previousnameshow').slideDown("fast");    
		else $('#previousnameshow').slideUp("fast"); // hide div if value is not "custom"				  
	});	  
	  
 
}); 
</script>
<link href="support/css/layout.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">

<div style=""><img src="images/avli-logo.png" width="203" height="54" title="AVLI logo"/></div>
<br />

  <h1>Student Enrolment Form (1/3)</h1>  
  <div id="logo"></div>
  <table width="100%" border="0">
    <tr>
      <td width="100%" valign="bottom"><h2>Personal Details</h2></td>
    </tr>
  </table>
  <form action="step2.php" method="post" name="firstpage" onsubmit="return validate_form();">
    <fieldset class="on">
      <table width="100%" border="0">
        <tr>
          <th>Title</th>
          <td><select name="ask_title" id="ask_title" title="Please select an option for Title">
            <option value="">Click to select an option</option>
            <option value="1"<?php check_selected("ask_title","1"); ?>>Mr</option>
            <option value="2"<?php check_selected("ask_title","2"); ?>>Mrs</option>
            <option value="3"<?php check_selected("ask_title","3"); ?>>Miss</option>
            <option value="4"<?php check_selected("ask_title","4"); ?>>Ms</option>
          </select></td>
        </tr>
        <tr>
          <th>First name</th>
          <td><input type="text" name="ask_firstname" id="ask_firstname" value="<?php check_for_content("ask_firstname"); ?>" title="Please enter your First name" /></td>
        </tr>
        <tr>
          <th>Last name</th>
          <td><input name="ask_lastname" type="text" id="ask_lastname" value="<?php check_for_content("ask_lastname"); ?>" title="Please enter your Last name" /></td>
        </tr>
        <tr>
          <th>Date of birth</th>
          <td><?php DateSelector("birthday"); ?></td>
        </tr>
        <tr>
          <th>Gender</th>
          <td><select name="ask_gender" id="ask_gender" title="Please select an option for Gender">
              <option value="">Click to select an option</option>
              <option value="1"<?php check_selected("ask_gender","1"); ?>>Male</option>
              <option value="2"<?php check_selected("ask_gender","2"); ?>>Female</option>
            </select></td>
        </tr>
        <tr>
          <th>Email Address</th>
          <td>
            <input type="text" name="ask_emailaddress" id="ask_emailaddress" title="Please enter your E-Mail Address in the proper format" value="<?php check_for_content("ask_emailaddress"); ?>" />
          </td>
        </tr>
        <tr>
          <th>Contact Number</th>
          <td>
            <input type="text" name="ask_contactnumber" id="ask_contactnumber" title="Please enter your contact number in the proper format" value="<?php check_for_content("ask_ask_contactnumber"); ?>" />
          </td>
        </tr>
      </table>
    </fieldset>
    <br />

     <table width="100%" border="0">
    <tr>
      <td width="100%" valign="bottom"><h2>NAME VET Course study</h2></td>
    </tr>
  </table>
    <fieldset class="odd">
      <table width="100%" border="0">
        <tr>
          <th>Vet course</th>
          <td><select name="ask_vetfeecourseslist" id="ask_vetfeecourseslist" title="Please select a course">
            <option value="">Click to select a course</option>
            <option value="1"<?php check_selected("ask_vetfeecourseslist","1"); ?>>BSB50207 Diploma of Business</option>
            <option value="2"<?php check_selected("ask_vetfeecourseslist","2"); ?>>BSB50407 Diploma of Business Administration</option>
            <option value="3"<?php check_selected("ask_vetfeecourseslist","3"); ?>>BSB50607 Diploma of Human Resources Management</option>
            <option value="4"<?php check_selected("ask_vetfeecourseslist","4"); ?>>BSB51107 Diploma of Management</option>
            <option value="5"<?php check_selected("ask_vetfeecourseslist","5"); ?>>BSB51407 Diploma of Project Management</option>
            <option value="6"<?php check_selected("ask_vetfeecourseslist","6"); ?>>BSB50207 Diploma of Business|BSB50613 Diploma of Human Resources Management</option>
            <option value="7"<?php check_selected("ask_vetfeecourseslist","7"); ?>>BSB50207 Diploma of Business|BSB51107 Diploma of Management</option>
            <option value="8"<?php check_selected("ask_vetfeecourseslist","8"); ?>>BSB50407 Diploma of Business Administration|BSB50207 Diploma of Business</option>
            <option value="9"<?php check_selected("ask_vetfeecourseslist","9"); ?>>BSB50407 Diploma of Business Administration|BSB51107 Diploma of Management</option>
            <option value="10"<?php check_selected("ask_vetfeecourseslist","10"); ?>>BSB50613 Diploma of Human Resources Management|BSB50407 Diploma of Business Administration</option>
            <option value="11"<?php check_selected("ask_vetfeecourseslist","11"); ?>>BSB50613 Diploma of Human Resources Management|BSB51107 Diploma of Management</option>
            <option value="12"<?php check_selected("ask_vetfeecourseslist","12"); ?>>BSB51413 Diploma of Project Management|BSB50207 Diploma of Business</option>
            <option value="13"<?php check_selected("ask_vetfeecourseslist","13"); ?>>BSB51413 Diploma of Project Management|BSB50407 Diploma of Business Administration</option>
            <option value="14"<?php check_selected("ask_vetfeecourseslist","14"); ?>>BSB51413 Diploma of Project Management|BSB50613 Diploma of Human Resources Management</option>
            <option value="15"<?php check_selected("ask_vetfeecourseslist","15"); ?>>BSB51413 Diploma of Project Management|BSB51107 Diploma of Management</option>
          </select></td>
        </tr>
      </table>
    </fieldset>

    <br />

      <table width="100%" border="0">
    <tr>
      <td width="100%" valign="bottom"><h2>Further information</h2></td>
    </tr>
  </table>
    <fieldset class="on">
      <table width="100%" border="0">
        <tr>
          <th>Do you have a previous name?</th>
          <td><select name="ask_previousname" id="ask_previousname" title="Please select whether you had a previous name or not" >
            <option value="">Click to select an option</option>
            <option value="1"<?php check_selected("ask_previousname","1"); ?>>Yes</option>
            <option value="2"<?php check_selected("ask_previousname","2"); ?>>No</option>
          </select></td>
        </tr>
      </table>
    </fieldset>


    <div class="invisible" id="previousnameshow">
    <div class="fieldtab">
    <h2>Previous Names</h2></div>
       <fieldset class="odd">
      <table width="100%" border="0">
        <tr>
          <th>First Name</th>
          <td><input type="text" name="ask_previousfirstname" title="Please enter your previous first name" id="previousask_firstname" value="" /></td>
        </tr>
        <tr>
          <th>Last Name</th>
          <td><input type="text" name="ask_previouslastname" title="Please enter your previous family name" id="previousask_lastname" value="" /></td>
        </tr>
        <tr>
          <th>Other Name</th>
          <td><input type="text" name="ask_previousothername" title="Please enter your previous other name/s" id="previousothername" value="" /></td>
        </tr>
      </table>
   
    </fieldset>
    </div>
    
    
    <br />
    
    
    
    <table width="100%" border="0">
      <tr>
        <td width="100%" valign="bottom"><h2>Home Address</h2></td>
      </tr>
    </table>
    <fieldset class="odd">
      <table width="100%" border="0">
        <tr>
          <th>Address</th>
          <td><input type="text" name="ask_address1" title="Please enter your Home Address" id="home_address" value="<?php check_for_content("fieldnamehere"); ?>" /></td>        
        </tr>
        <tr>
          <th>Suburb / Town</th>
          <td><input type="text" name="ask_addresssuburb1" title="Please enter your Home Address - Suburb / Town" id="home_suburb" value="<?php check_for_content("ask_addresssuburb1"); ?>" /></td>
        </tr>
        <tr>
          <th>City</th>
          <td><input type="text" class="donotval" name="ask_addresscity1" title="Please enter your Home Address - City" id="home_city" value="<?php check_for_content("ask_addresscity1"); ?>" /></td>
        </tr>
        <tr>
          <th>State</th>
          <td>
          
          <select name="ask_addressstate1" title="Please enter your Home Address - State" id="home_state" >
              <option value="">Click to select a state</option>
              <option value="8"<?php check_selected("ask_addressstate1","8"); ?>>Australian Capital Territory</option>
              <option value="2"<?php check_selected("ask_addressstate1","2"); ?>>New South Wales</option>
              <option value="3"<?php check_selected("ask_addressstate1","3"); ?>>Northern Territory</option>
              <option value="4"<?php check_selected("ask_addressstate1","4"); ?>>Queensland</option>
              <option value="8"<?php check_selected("ask_addressstate1","5"); ?>>South Australia</option>
              <option value="6"<?php check_selected("ask_addressstate1","6"); ?>>Tasmania</option>
              <option value="1"<?php check_selected("ask_addressstate1","1"); ?>>Victoria</option>
              <option value="7"<?php check_selected("ask_addressstate1","7"); ?>>Western Australia</option>
              
            </select>
          
          
          </td>
        </tr>
        <tr>
          <th>Postcode</th>
          <td><input type="text" name="ask_addresspostcode1" title="Please enter your Home Address - Postcode" id="home_postcode" value="<?php check_for_content("ask_addresspostcode1"); ?>" /></td>
        </tr>
        <tr>
          <th>Country</th>
          <td>
            <select name="ask_addresscountry1" id="home_country" title="Please select a Home Address - Country">
              <option value="">Click to select an Country</option>
              <?php generate_country_list("ask_addresscountry1","txt"); ?>
            </select>
          </td>
        </tr>
      </table>
    </fieldset>
    <br />
    <table width="100%" border="0">
      <tr>
        <td width="100%" valign="bottom"><div style="position: relative;">
            <h2>Postal Address</h2>
            <a href="#" onclick="javascript:auto_fill();" title="" id="asabove" style="position: absolute; top: -8px; right: 0px;">Click to fill postal address as above if same</a></div></td>
      </tr>
    </table>
    <fieldset class="on">
      <table width="100%" border="0">
        <tr>
          <th valign="top">Address</th>
          <td><input name="ask_address2" type="text" class="spacing" id="postal_address" title="Please enter your Postal Address" value="<?php check_for_content("ask_address2"); ?>" /></td>
        </tr>
        <tr>
          <th>Suburb / Town</th>
          <td><input type="text" name="ask_addresssuburb2" title="Please enter your Postal Address - Suburb / Town" id="postal_suburb" value="<?php check_for_content("ask_addresssuburb2"); ?>" /></td>
        </tr>
        <tr>
          <th>City</th>
          <td><input type="text" class="donotval" name="ask_addresscity2" title="Please enter your Postal Address - City" id="postal_city" value="<?php check_for_content("ask_addresscity2"); ?>" /></td>
        </tr>
        <tr>
          <th>State</th>
          <td>
            <select name="ask_addressstate2" title="Please enter your Postal Address - State" id="postal_state" >
                <option value="">Click to select a state</option>
                <option value="8"<?php check_selected("ask_addressstate1","8"); ?>>Australian Capital Territory</option>
                <option value="2"<?php check_selected("ask_addressstate1","2"); ?>>New South Wales</option>
                <option value="3"<?php check_selected("ask_addressstate1","3"); ?>>Northern Territory</option>
                <option value="4"<?php check_selected("ask_addressstate1","4"); ?>>Queensland</option>
                <option value="8"<?php check_selected("ask_addressstate1","5"); ?>>South Australia</option>
                <option value="6"<?php check_selected("ask_addressstate1","6"); ?>>Tasmania</option>
                <option value="1"<?php check_selected("ask_addressstate1","1"); ?>>Victoria</option>
                <option value="7"<?php check_selected("ask_addressstate1","7"); ?>>Western Australia</option>
                
              </select>
          </td>
        </tr>
        <tr>
          <th>Postcode</th>
          <td><input type="text" name="ask_addresspostcode2" title="Please enter your Postal Address - Postcode" id="postal_postcode" value="<?php check_for_content("ask_addresspostcode2"); ?>" /></td>
        </tr>
        <tr>
          <th>Country</th>
          <td>
            <select name="ask_addresscountry2" id="postal_country" title="Please select a Postal Address - Country">
              <option value="">Click to select an Country</option>
              <?php generate_country_list("ask_addresscountry2","txt"); ?>
            </select>
          </td>
        </tr>
      </table>
    </fieldset>
    <br />
    <p align="right" style="padding-right: 34px;">
      <input type="submit" value="Continue to Page 2 &raquo;" name="submit" class="submit" />
    </p>
    <br />
    <br />
  </form>
</div>
<hr class="general" />

<script type="text/javascript" src="support/js/auto-fill.js"></script>
</body>
</html>
